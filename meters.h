/*
Copyright (C) Amina Guermouche 2019.
Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
(See accompanying file LICENSE file or copy at http://opensource.org/licenses/MIT)
*/

#pragma once
#include <unistd.h>
#include "duf.h"

/** Defines shorthands for the different meters. */
enum METERS
{
  METER_FLOPS,
  METER_MEM_BW,
  METER_L3_BW,
  METER_PKG_PWR,
  METER_DRAM_PWR,
  METER_OP_INT,
  METER_UFREQ,
  METER_FREQ,
  METER_POWERCAP_C0,
  METER_POWERCAP_C1,
  NB_METERS, //must be kept at the end to get the right number of enum definitions
};

/** Number of meters defined for one uncore. */
static const int N_METERS = NB_METERS;

/** prints interleaved meters header */
void
print_meters_header();

/** prints interleaved meter data */
void
print_meters(struct data* data, double t);

/** get a meter value given an uncore index and a meter shorthand */
double
get_meter(struct data* data, enum METERS meter);

/** set a meter value given an uncore index and a meter shorthand */
void
set_meter(struct data* data, enum METERS meter, double value);
