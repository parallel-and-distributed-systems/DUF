/*
Copyright (C) Amina Guermouche 2021.
Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
(See accompanying file LICENSE file or copy at http://opensource.org/licenses/MIT)
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>
#include <fcntl.h>
#include "powercap_access.h"

//const char *powercap_files_names[N_PWRC_EVTS]     = {"constraint_0_power_limit_uw", "constraint_1_power_limit_uw"};
extern powercap_rapl_pkg * pkgs;

int init_powercap(void)
{
  uint32_t count = powercap_rapl_get_num_instances();
  if (count == 0) {
    // none found (maybe the kernel module isn't loaded?)
    perror("powercap_rapl_get_num_instances");
      return -1;
  }
  pkgs = malloc(count * sizeof(powercap_rapl_pkg));
  uint32_t i;
  for (i = 0; i < count; i++) {
    if (powercap_rapl_init(i, &pkgs[i], 0)) {
      // could be that you don't have write privileges
      perror("powercap_rapl_init");
      return -1;
    }
  }
  return 0;
}


long long read_powercap( int socket, powercap_rapl_constraint constraint)
{
  uint64_t val;
  powercap_rapl_get_power_limit_uw(&pkgs[socket], POWERCAP_RAPL_ZONE_PACKAGE, constraint, &val);
  return val * 1e-6;
}

int write_powercap( int socket, int constraint, uint64_t value )
{
  powercap_rapl_set_power_limit_uw(&pkgs[socket], POWERCAP_RAPL_ZONE_PACKAGE, constraint, value * 1e6);
  return 1;
}

void
destroy_powercap(int* powercap_fds)
{
  for(int i = 0; i < N_PWRC_EVTS; i++)
    if (powercap_fds[i] > 0)
      {
	close(powercap_fds[i]);
      }
}

