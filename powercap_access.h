/*
Copyright (C) Amina Guermouche 2021.
Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
(See accompanying file LICENSE file or copy at http://opensource.org/licenses/MIT)
*/

#ifndef _POWERCAP_ACCESS_H
#define _POWERCAP_ACCESS_H

#include <powercap/powercap-rapl.h>

#define N_PWRC_EVTS 2
#define N_SOCKETS 2
#define MAX_LENGTH 128

static const powercap_rapl_constraint constraints[] = { POWERCAP_RAPL_CONSTRAINT_LONG, POWERCAP_RAPL_CONSTRAINT_SHORT };
//void init_powercap(int* powercap_fd, int socket);
//long long read_powercap( int socket, int constraint );
//int write_powercap( int socket, int constraint , long long value );
int init_powercap(void);
long long read_powercap( int socket, powercap_rapl_constraint constraint);
int write_powercap( int socket, int constraint, uint64_t value );
//void destroy_powercap(int* powercap_fd);
  
#endif
