/*
Copyright (C) Amina Guermouche 2019.
Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
(See accompanying file LICENSE file or copy at http://opensource.org/licenses/MIT)
*/

#include <stdio.h>
#include <unistd.h>
#include "meters.h"

/** Defines the header names of these meters */
static const char* METER_NAMES[] = {
  "MFLOP/s",        "MemoryBandwidth[MB/s]", "L3Bandwidth[MB/s]",  "PackagePower[W]",
  "DRAMPower[W]", "OperationalIntensity",  "UncoreFrequency[MHz]", "CPUFrequency[Mhz]", "PowercapConstraint0[W]", "PowercapConstraint1[W]"
};


void
print_meters_header()
{
  printf("Time [s]\t");
  for (int m = 0; m < (int)N_METERS; m++) {
    //    for (int u = 0; u < data->uncores; u++) {
    //      printf("%s %d\t", METER_NAMES[m], u);
      printf("%s \t", METER_NAMES[m]);
      // }
  }
  printf("\n");
}

/** prints interleaved meters */
void
print_meters(struct data* data, double t)
{
  printf(FLOAT_PRECISION_FMT "\t", t);
  for (int m = 0; m < (int)N_METERS; m++) {
    //    for (int u = 0; u < data->uncores; u++) {
    //      printf(FLOAT_PRECISION_FMT "\t", data->meters[m + u * N_METERS]);
    printf(FLOAT_PRECISION_FMT "\t", data->meters[m]);
      //    }
  }
  printf("\n");
}

double
get_meter(struct data* data, enum METERS meter)
{
  return data->meters[ meter ];
}

void
set_meter(struct data* data, enum METERS meter, double value)
{
  data->meters[ meter ] = value;
}
