/*
Copyright (C) Amina Guermouche 2019.
Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
(See accompanying file LICENSE file or copy at http://opensource.org/licenses/MIT)
*/

#pragma once
#include <stdbool.h>

struct arguments
{
  unsigned int sleep_usec;
  char* uncore_list;
  unsigned int perf_loss;
  bool verbose;
  bool cpu_frequency;
  bool power;
  bool measurement;
  bool powercapping;
};

