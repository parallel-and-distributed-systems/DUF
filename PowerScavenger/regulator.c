#include <likwid.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "regulator.h"
#include "meters.h"
#include "arch/event.h"


#define K_ERROR 0.95
#define STEP_UFREQ 100
static enum phase { CPU, MEM } phase;
extern int verbose;
static int ufreq;
static double ipc;
static double old_ipc;
static double dram_power;
static double set_point_dram_power;


enum action
  {
    RESET_UFREQ,
    INC_UFREQ,
    DEC_UFREQ,
  };

int read_msr(int msr_fd,  uint32_t uncore_register)
{
  ssize_t ret;
  uint64_t freq = 0;
  if (msr_fd > 0)
    {
      ret = pread(msr_fd, &freq, sizeof(uint64_t), uncore_register);
      if (ret < 0)
        {
	  fprintf(stderr,"Cannot read uncore frequency register\n");
	  if (msr_fd > 0)
	    {
	      close(msr_fd);
	    }
	  exit(0);
        }
      else if (ret != sizeof(uint64_t))
        {
	  fprintf(stderr, "Incomplete read of uncore frequency register \n");
	  if (msr_fd > 0)
	    {
	      close(msr_fd);
	    }
	  exit(0);
        }
    }
  return freq;
}

int write_msr(uint64_t freq, int msr_fd)
{
  ssize_t ret;
  uint32_t uncore_register = 0x620;
  if (msr_fd > 0)
    {
      ret = pwrite(msr_fd, &freq, sizeof(uint64_t), uncore_register);
      if (ret < 0)
        {
	  fprintf(stderr, "Cannot write uncore frequency register\n");
	  if (msr_fd > 0)
	    {
	      close(msr_fd);
	    }
	  exit(0);
        }
      else if (ret != sizeof(uint64_t))
        {
	  fprintf(stderr, "Incomplete read on register\n");
	  if (msr_fd > 0)
	    {
	      close(msr_fd);
	    }
	  exit(0);
        }
    }
  return 1;
}

static void set_min_ufreq (int freq, int msr_fd){
  uint64_t f = freq / 100;
  uint32_t uncore_register = 0x620;
  uint64_t tmp = read_msr(msr_fd, uncore_register);
  tmp &= ~(0xFF00);
  tmp |= (f<<8);
  write_msr(tmp, msr_fd);
}

static void set_max_ufreq (int freq, int msr_fd){
  uint64_t f = freq / 100;
  uint32_t uncore_register = 0x620;
  uint64_t tmp = read_msr(msr_fd, uncore_register);
  tmp &= ~(0xFFULL);
  tmp |= (f & 0xFFULL);
  write_msr(tmp, msr_fd);
}

static void
set_ufreq(int f, int msr_fd)
{
  DEBUGP("set uncore freq %d\n", f);
  set_min_ufreq(f, msr_fd);
  set_max_ufreq(f, msr_fd);
  ufreq = f;
}

/** Resets the uncore frequency by un-constraining it. Updates the ufreq array
    too. in ufreq, set it to max_ufreq, because for our purposes it's the same
    thing. */
static void
reset_ufreq(int msr_fd)
{
  DEBUGP("reset ufreq\n");
  set_min_ufreq(MIN_UFREQ, msr_fd);
  set_max_ufreq(MAX_UFREQ, msr_fd);
  ufreq = MAX_UFREQ;
}

static int
get_ufreq(int msr_fd)
{
  DEBUGP("getting current uncore frequency\n");
  uint32_t uncore_register = 0x621;
  uint64_t ufreq = read_msr(msr_fd, uncore_register);
  ufreq = (ufreq & 0xFFULL) * 100;
  return(ufreq);
}

void
regulator_init(int socket_idx, int *msr_fd)
{
  phase = -1;
  old_ipc = -1.0;
  ipc = -1.0;
  ufreq = 0;
  dram_power = 0.0;
  set_point_dram_power = 0.0;
  char* msr_name = (char*)malloc(15);
  sprintf(msr_name, "/dev/cpu/%d/msr", socket_idx);
  *msr_fd = open(msr_name, O_RDWR);
  if (*msr_fd < 0)
    {
      fprintf(stderr,"Cannot open MSR file %s\n", msr_name);
      exit(0);
    }
  free(msr_name);
  reset_ufreq(*msr_fd);
}

void
regulator_destroy(int msr_fd)
{
  reset_ufreq(msr_fd);
  if (msr_fd > 0)
    {
      close(msr_fd);
    }
}

static enum action
decide_action(double ipc, double dram)
{
  if (dram > 1.05 * set_point_dram_power) /*DRAM > setPoint : phase trasition C -> M*/
    {
      phase = MEM;
      set_point_dram_power = dram;
      DEBUGP("phase change from CPU to memory : reset\n");
      return RESET_UFREQ;
    }
  else {
    if ((dram >= 0.95 * set_point_dram_power) && (dram <= 1.05 * set_point_dram_power)) /* DRAM == setPoint we can reduce frequency*/
      {
	DEBUGP("Steady state in DRAM : decrease\n");
	return DEC_UFREQ;
      }
    else
      if (dram < 0.95 * set_point_dram_power){ /* DRAM < setPoint*/
	if (ipc < 0.95 * old_ipc) /* ipc < old_ipc : performance degradation*/
	  {
	    DEBUGP("Peformance degradation : increase\n");
	    return INC_UFREQ;
	  }
	else /* phase change M -> C*/
	  {
	    phase = CPU;
	    set_point_dram_power = dram;
	    DEBUGP("phase change from memory to CPU : reset\n");
	    return RESET_UFREQ;
	  }
      }
  }
}

void
regulate(struct data* data, int socket_idx, int msr_fd)
{
  DEBUGP("Socket #%d: ", socket_idx);
  double ipc = get_meter(data, METER_IPC);
  double dramPower = get_meter(data, METER_DRAM_PWR);
  int cur_freq =  get_ufreq(msr_fd);
  //    double power = get_meter(data, u, METER_PKG_PWR);
  //int cur_freq =  get_ufreq(msr_fd);
  //    data->meters[ METER_FREQ] =  freq_getCpuClockCurrent(socket_idx) / 1000000;
  DEBUGP("requested ufreq %d, set ufreq %d\n", ufreq,  cur_freq);
  ufreq = cur_freq;
  /* The following lines, until decide_action call, are here to handle default UFS*/
  /* if(handling_default_ufs) */
  /* { */
  /*    int cur_freq =  freq_getUncoreFreqCur(socket_idx); */
  /*    DEBUGP("requested ufreq %d, set ufreq %d\n", ufreq[0],  cur_freq); */
  /*    if((power / TDP > K_ERROR) && (cur_freq != ufreq[u])) */
  /*    { */
  /* 	 DEBUGP("At TDP %f W, we have to follow UFS, setting frequency to %d\n", power, cur_freq); */
  /* 	 ufreq[u] = cur_freq; */
  /*    } */
  /*    else */
  /* 	 if ((power / TDP < K_ERROR) && (cur_freq != MAX_UFREQ)) */
  /* 	 { */
  /*         reset_ufreq(u, socket_idx); */
  /* 	    DEBUGP("The power decreased %f compared to TDP, let's reset uncore freq to %d\n", power, ufreq[u]); */
  /* 	    return; */
  /* 	 } */
  /* } */
  enum action action = decide_action(ipc, dramPower);
  switch(action){
  case RESET_UFREQ :
    DEBUGP("Reset: ");
    set_ufreq(MAX_UFREQ, msr_fd);
    data->meters[ METER_UFREQ] =  ufreq;
    break;
  case INC_UFREQ :
    if (ufreq + STEP_UFREQ <= MAX_UFREQ) {
      DEBUGP("increase: ");
      set_ufreq(ufreq + STEP_UFREQ, msr_fd);
      data->meters[ METER_UFREQ] =  ufreq;
    }
    break;
  case DEC_UFREQ :
    if (ufreq - STEP_UFREQ >= MIN_UFREQ) {
      DEBUGP("decrease: ");
      set_ufreq(ufreq - STEP_UFREQ, msr_fd);
      data->meters[ METER_UFREQ] =  ufreq;
    }
    break;
  }
  old_ipc = ipc;
  
}
