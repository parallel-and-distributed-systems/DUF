#pragma once
#include <argp.h>
#include <stdbool.h>

struct arguments
{
  unsigned int sleep_usec;
  char* uncore_list;
  unsigned int perf_loss;
  bool verbose;
  bool default_ufs_handling;
};
struct argp argp;
