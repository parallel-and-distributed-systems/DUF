#include <stdio.h>
#include <unistd.h>
#include "meters.h"

void
print_meters_header()
{
  printf("Time [s]\t");
  for (int m = 0; m < (int)N_METERS; m++) {
      printf("%s\t", METER_NAMES[m]);
  }
  printf("\n");
}

/** prints interleaved meters */
void
print_meters(struct data* data, double t)
{
  printf(FLOAT_PRECISION_FMT "\t", t);
  for (int m = 0; m < (int)N_METERS; m++) {
      printf(FLOAT_PRECISION_FMT "\t", data->meters[m]);
  }
  printf("\n");
}

double
get_meter(struct data* data, enum METERS meter)
{
  return data->meters[meter];
}

void
set_meter(struct data* data, enum METERS meter, double value)
{
  data->meters[meter] = value;
}
