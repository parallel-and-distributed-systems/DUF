#pragma once
#include <unistd.h>
#include "PS.h"

/** Defines shorthands for the different meters. */
enum METERS
{
  METER_IPC,
  METER_PKG_PWR,
  METER_DRAM_PWR,
  METER_UFREQ,
  METER_FREQ
};
/** Defines the header names of these meters */
static const char* METER_NAMES[] = {
  "IPC", "Package Power [W]",
  "DRAM_Power [W]",  "Uncore Frequency [MHz]", "CPU Frequency [MHz]"
};
/** Number of meters defined for one uncore. */
static const int N_METERS = sizeof(METER_NAMES) / sizeof(char*);

/** prints interleaved meters header */
void
print_meters_header();

/** prints interleaved meter data */
void
print_meters(struct data* data, double t);

/** get a meter value given an uncore index and a meter shorthand */
double
get_meter(struct data* data, enum METERS meter);

/** set a meter value given an uncore index and a meter shorthand */
void
set_meter(struct data* data, enum METERS meter, double value);
