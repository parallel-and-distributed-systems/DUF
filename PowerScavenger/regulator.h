#include <stdint.h>
#include "PS.h"
/** Inits the regulator and resets uncore frequency on all uncores.
 * @param n_uncores number of uncores that will be metered.
 */
void
regulator_init(int socket_idx, int *msr_fd);

/** Destroys the regulator and resets uncore frequency on all uncores */
void
regulator_destroy(int msr_fd);

/** Regulates the uncores by reading meter_val from data.
    This needs to be called after calculating the meters.
*/
void
regulate(struct data* data, int socket_idx, int msr_fd);
