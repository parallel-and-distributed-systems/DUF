#include "../PS.h"
#include "../meters.h"
#include "event.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

enum CPU_EVTS
{
  EVT_TOT_CYCLES,
  EVT_TOT_INS
};
const char* CPU_EVT_NAMES[] = { "CPU_CLK_THREAD_UNHALTED:THREAD_P:cpu=%d",
                                "INST_RETIRED:ANY_P:cpu=%d"};

enum UNCORE_EVTS
{
  EVT_PKG_ENERGY,
  EVT_DRAM_ENERGY,
};

const char* UNCORE_EVT_NAMES[] = { "rapl::RAPL_ENERGY_PKG:cpu=%d",
                                   "rapl::RAPL_ENERGY_DRAM:cpu=%d"};

const int N_CPU_EVTS = sizeof(CPU_EVT_NAMES) / sizeof(char*);
const int N_UNCORE_EVTS = sizeof(UNCORE_EVT_NAMES) / sizeof(char*);

const int MIN_UFREQ = 1200;
const int MAX_UFREQ = 2700;


void
calculate_meters(struct data* data, double dt)
{
  double cpu_nrj, dram_nrj;
  double cycles = 0, instructions = 0;
  int cpu_val_off = 0;
  int uncore_val_off = 0;
  int meters_off = 0;
  for (int i = 0; i < N_CPU_EVTS * data->cpus;
       i += N_CPU_EVTS) {
    cycles += data->cpu_val[cpu_val_off + i + EVT_TOT_CYCLES];
    instructions += data->cpu_val[cpu_val_off + i + EVT_TOT_INS];
  }
  // cpu and dram energy, unit is 2^-32 Joules
  cpu_nrj = data->uncore_val[uncore_val_off + EVT_PKG_ENERGY];
  dram_nrj = data->uncore_val[uncore_val_off + EVT_DRAM_ENERGY];

  // number of read and write instructions
  // multiply by 64 (size of cach line), cf. somewhere in the intel docs
  // populate meters array
  data->meters[meters_off + METER_IPC] =
    instructions / cycles;
  data->meters[meters_off + METER_PKG_PWR] =
    cpu_nrj * pow(2, -32) / dt;
  data->meters[meters_off + METER_DRAM_PWR] =
    dram_nrj * pow(2, -32) / dt;
}

