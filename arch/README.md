# Architecture files
## Description
In each file, architecture specific meters are defined:

 - CPU_EVT_NAMES: The papi event names for the CPU events. DUF needs the flops (scalar, SSE, AVX2 and AVX512 when available). It also needs the level 3 cache events.
 - CPU_EVTS: How each counter is named and used in DUF.
 - UNCORE_EVT_NAMES: The papi events for uncore counters. DUF needs power measurements (with papi), and memory bandwidth counters.
 - UNCORE_EVT_NAMES: How the uncore events are named in our code.

The function ` calculate_meters(struct data* data, double dt) ` computes the flops, the memory bandwidth and the power consumption as needed by DUF.

DUF also needs the minimum and maximum uncore frequency for the target platform.

## New architecture
For now creating a new architecture file is done manually. An automatic generation is a work in progress.

In order to get the right events, one must check with
papi_native_avail if the events in one of the provided files are the
same and update/change them otherwise. The script get_events.sh is
able to detect flops and bandwidth events on most recent Intel
arhcitectures. Please do not forget to update the formula for flops
and memory bandwidth computation. Note that L3 and power events have
the same name on all the platfomrs that we tested so far. As a
consequence, the script get_events.sh does not look for such events.

Finally, the maximum and minimum uncore frequency may vary from one
architecture to another. It also depends on the maximum and minimum
CPU frequency. As a consequence, for the architectures that we
provide, we obtained the minimum and maximum uncore frequency
empirically by trying to set different uncore frequency values (using
[LIKWID](https://github.com/RRZE-HPC/likwid) for instance) and by comparing the
actually set value to the one that we requested.