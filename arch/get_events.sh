#!/bin/bash
## get events for this machine
papi_native_avail > events ## this may take some time

## get flops events
if grep "FP_ARITH " events >/dev/null; then
    echo "flops events"
    if grep SCALAR_DOUBLE events >/dev/null; then
	echo "FP_ARITH:SCALAR_DOUBLE:cpu=%d"
    fi
    if grep 128B_PACKED_DOUBLE events >/dev/null; then
	echo "FP_ARITH:128B_PACKED_DOUBLE:cpu=%d"
    fi
    if grep 256B_PACKED_DOUBLE events >/dev/null; then
	echo "FP_ARITH:256B_PACKED_DOUBLE:cpu=%d"
    fi
    if grep 512B_PACKED_DOUBLE events >/dev/null; then
	echo "FP_ARITH:512B_PACKED_DOUBLE:cpu=%d"
    fi
else
    echo "The name of the flops events on this machine do not seem to be call FP_ARITH:*, please check the names with papi_native_avail"
fi

## get memory events
if grep "UNC_M_CAS_COUNT " events >/dev/null; then
    echo "memory bandwidth events"
    grep "UNC_M_CAS_COUNT " events | awk 'BEGIN{FS = "|" }; {print $2}' > tmp
    while read event;
    do
	echo $event::RD:cpu=%d
	echo $event::WR:cpu=%d 
    done<tmp
else
    echo "Memory bandwidth events names may not containt UNC_M_CAS_COUNT, please check manually"
fi

## clean 
rm events
rm tmp
