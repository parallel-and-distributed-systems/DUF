/*
Copyright (C) Amina Guermouche 2019.
Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
(See accompanying file LICENSE file or copy at http://opensource.org/licenses/MIT)
*/

#include "../duf.h"
#include "../meters.h"
#include "event.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

enum CPU_EVTS
{
  EVT_DOUBLE,
  EVT_DOUBLE_128,
  EVT_DOUBLE_256,
  EVT_DOUBLE_512,
  EVT_L2_TRANS,
  EVT_L2_LINES,
};
const char* CPU_EVT_NAMES[] = { "FP_ARITH:SCALAR_DOUBLE:cpu=%d",
                                "FP_ARITH:128B_PACKED_DOUBLE:cpu=%d",
                                "FP_ARITH:256B_PACKED_DOUBLE:cpu=%d",
				"FP_ARITH:512B_PACKED_DOUBLE:cpu=%d",
				"L2_TRANS:L2_WB:cpu=%d",
				"L2_LINES_IN:ALL:cpu=%d"};

enum UNCORE_EVTS
{
  EVT_PKG_ENERGY,
  EVT_DRAM_ENERGY,
  EVT_CAS_RD_0,
  EVT_CAS_WR_0,
  EVT_CAS_RD_1,
  EVT_CAS_WR_1,
  EVT_CAS_RD_2,
  EVT_CAS_WR_2,
  EVT_CAS_RD_3,
  EVT_CAS_WR_3,
  EVT_CAS_RD_4,
  EVT_CAS_WR_4,
  EVT_CAS_RD_5,
  EVT_CAS_WR_5,
};

const char* UNCORE_EVT_NAMES[] = { "rapl::RAPL_ENERGY_PKG:cpu=%d",
                                   "rapl::RAPL_ENERGY_DRAM:cpu=%d",
                                   "skx_unc_imc0::UNC_M_CAS_COUNT:RD:cpu=%d",
                                   "skx_unc_imc0::UNC_M_CAS_COUNT:WR:cpu=%d",
                                   "skx_unc_imc1::UNC_M_CAS_COUNT:RD:cpu=%d",
                                   "skx_unc_imc1::UNC_M_CAS_COUNT:WR:cpu=%d",
				   "skx_unc_imc2::UNC_M_CAS_COUNT:RD:cpu=%d",
                                   "skx_unc_imc2::UNC_M_CAS_COUNT:WR:cpu=%d",
                                   "skx_unc_imc3::UNC_M_CAS_COUNT:RD:cpu=%d",
                                   "skx_unc_imc3::UNC_M_CAS_COUNT:WR:cpu=%d",
                                   "skx_unc_imc4::UNC_M_CAS_COUNT:RD:cpu=%d",
                                   "skx_unc_imc4::UNC_M_CAS_COUNT:WR:cpu=%d",
                                   "skx_unc_imc5::UNC_M_CAS_COUNT:RD:cpu=%d",
                                   "skx_unc_imc5::UNC_M_CAS_COUNT:WR:cpu=%d" };

const int N_CPU_EVTS = sizeof(CPU_EVT_NAMES) / sizeof(char*);
const int N_UNCORE_EVTS = sizeof(UNCORE_EVT_NAMES) / sizeof(char*);

const int MIN_UFREQ = 1200;
const int MAX_UFREQ = 2400;
const int TDP = 125;

void
calculate_meters(struct data* data, double dt)
{
  double flop, datavolume;
  double cpu_nrj, dram_nrj;
  double l2_in, l2_trans;

  int cpu_val_off = 0;
  int uncore_val_off = 0;
  int meters_off = 0;
  flop = 0;
  l2_in = 0;
  l2_trans = 0;
  for (int i = 0; i < N_CPU_EVTS * data->cpus;
       i += N_CPU_EVTS) {
    flop += data->cpu_val[cpu_val_off + i + EVT_DOUBLE] +
      data->cpu_val[cpu_val_off + i + EVT_DOUBLE_128] * 2.0 +
      data->cpu_val[cpu_val_off + i + EVT_DOUBLE_256] * 4.0 +
      data->cpu_val[cpu_val_off + i + EVT_DOUBLE_512] * 8.0;
    l2_trans += data->cpu_val[cpu_val_off + i + EVT_L2_TRANS];
    l2_in += data->cpu_val[cpu_val_off + i + EVT_L2_LINES];
  }
  // cpu and dram energy, unit is 2^-32 Joules
  cpu_nrj = data->uncore_val[uncore_val_off + EVT_PKG_ENERGY];
  dram_nrj = data->uncore_val[uncore_val_off + EVT_DRAM_ENERGY];

  // number of read and write instructions
  // multiply by 64 (size of cach line), cf. somewhere in the intel docs
  datavolume = (data->uncore_val[uncore_val_off + EVT_CAS_RD_0] +
		data->uncore_val[uncore_val_off + EVT_CAS_WR_0] +
		data->uncore_val[uncore_val_off + EVT_CAS_RD_1] +
		data->uncore_val[uncore_val_off + EVT_CAS_WR_1] +
		data->uncore_val[uncore_val_off + EVT_CAS_RD_2] +
		data->uncore_val[uncore_val_off + EVT_CAS_WR_2] +
		data->uncore_val[uncore_val_off + EVT_CAS_RD_3] +
		data->uncore_val[uncore_val_off + EVT_CAS_WR_3] +
		data->uncore_val[uncore_val_off + EVT_CAS_RD_4] +
		data->uncore_val[uncore_val_off + EVT_CAS_WR_4] +
		data->uncore_val[uncore_val_off + EVT_CAS_RD_5] +
		data->uncore_val[uncore_val_off + EVT_CAS_WR_5]) *
    64;
  // populate meters array
  data->meters[meters_off + METER_FLOPS] =
    flop * pow(10, -6) / dt; // [MFLOP count] / [s]
  data->meters[meters_off + METER_MEM_BW] =
    datavolume * pow(10, -6) / dt; // [MB] / [s] (base 10)
  data->meters[meters_off + METER_L3_BW] =
    (l2_trans + l2_in) * pow(10, -6) * 64.0 / dt; // [MB] / [s] (base 10)
  data->meters[meters_off + METER_PKG_PWR] =
    cpu_nrj * pow(2, -32) / dt; // [J] / [s] = [W]
  data->meters[meters_off + METER_DRAM_PWR] =
    dram_nrj * pow(2, -32) / dt;                               // same
  data->meters[meters_off + METER_OP_INT] = flop / datavolume; // [No unit]
}

