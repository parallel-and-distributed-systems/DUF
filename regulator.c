/*
  Copyright (C) Amina Guermouche 2019.
  Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
  (See accompanying file LICENSE file or copy at http://opensource.org/licenses/MIT)
*/

//#include <likwid.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <string.h>
#include <errno.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "regulator.h"
#include "meters.h"
#include "arch/event.h"
#include "msr_access.h"
#include "powercap_access.h"

/** Minimum MFLOPs threshold for activation */
#define MIN_MFLOPS 1
/** If current mflops is over K_ERROR*maxmflops, consider we are within an
 * acceptable performance margin */
#define K_ERROR 0.98
// TODO define these in arch-specific files, or better yet, detect it in
// initialization (not always possible)
/** Step when modifying uncore frequency */
#define STEP_UFREQ 100

extern int verbose;
extern int cpu_frequency;
extern int power;
extern int powercapping;
extern long long int default_powercap[N_PWRC_EVTS];
extern int powercap_iter;

enum action
{
  DO_NOTHING,
  RESET_UFREQ,
  INC_UFREQ,
  DEC_UFREQ,
  SET_MIN_FREQ,
  INC_PWRC
};
/** These arrays contain the state of each uncore */
/** Detected phase */
static enum phase { UNK, CPU, MEM } phase;
/** Measured maximum mflops */
static double maxmflops;
static double max_bw;
static double max_l3;
/* Current uncore frequency */
static int ufreq;
static int old_ufreq;
static double old_flops;
static double old_oi;
static  int old_decision;
static long long int cur_powercap;
static long long int cur_powercap1;
static double old_l3 = 0;

static int deactivate_powercap = 0;/*This variable is used for highly
									 CPU intensive applications. When
									 the powercap has an impact on
									 performance, we reset the
									 powercap in order to avoid to
									 much impact.*/
static int reset_pc = 0;
static int retry_reset = 0;
static int no_decrease = 0; /* This variable is used if the current
							   power consumption is larger than
							   powercap, then we reset_powercap, and
							   we forbid DUF from decreasing powercap
							   for this iteration if this is what the
							   regulator decided*/
static int just_reset_powercap = 0;
/** Sets the uncore frequency by constraining it. Updates the ufreq array too.
 */


static void set_min_ufreq (int freq, int msr_fd){
  uint64_t f = freq / 100;
  uint32_t uncore_register = 0x620;
  uint64_t tmp = read_msr(msr_fd, uncore_register);
  tmp &= ~(0xFF00);
  tmp |= (f<<8);
  write_msr(tmp, msr_fd);
}

static void set_max_ufreq (int freq, int msr_fd){
  uint64_t f = freq / 100;
  uint32_t uncore_register = 0x620;
  uint64_t tmp = read_msr(msr_fd, uncore_register);
  tmp &= ~(0xFFULL);
  tmp |= (f & 0xFFULL);
  write_msr(tmp, msr_fd);
}

static void
set_ufreq(int f, int msr_fd)
{
  DEBUGP("set uncore freq %d\n", f);
  set_min_ufreq(f, msr_fd);
  set_max_ufreq(f, msr_fd);
  ufreq = f;
}


/** Resets the uncore frequency by un-constraining it. Updates the ufreq array
    too. in ufreq, set it to max_ufreq, because for our purposes it's the same
    thing. */
static void
reset_ufreq(int msr_fd)
{
  DEBUGP("reset ufreq\n");
  set_min_ufreq(MIN_UFREQ, msr_fd);
  set_max_ufreq(MAX_UFREQ, msr_fd);
  ufreq = MAX_UFREQ;
}


static int
get_ufreq(int msr_fd)
{
  DEBUGP("getting current uncore frequency\n");
  uint32_t uncore_register = 0x621;
  uint64_t ufreq = read_msr(msr_fd, uncore_register);
  ufreq = (ufreq & 0xFFULL) * 100;
  return(ufreq);
}

/* This code is extracted from likwid*/
uint64_t get_freq(const int cpu_id )
{
  FILE *f = NULL;
  char cmd[256];
  char buff[256];
  char* eptr = NULL;
  uint64_t clock = 0x0ULL;
  sprintf(buff, "/sys/devices/system/cpu/cpu%d/cpufreq/scaling_cur_freq", cpu_id);
  f = fopen(buff, "r");
  if (f == NULL) {
    fprintf(stderr, "Unable to open path %s for reading\n", buff);
    return 0;
  }
  eptr = fgets(cmd, 256, f);
  if (eptr != NULL)
  {
	clock = strtoull(cmd, NULL, 10);
  }
  fclose(f);
  return clock * 1E3;
}

void
regulator_init(int *msr_fd)
{
  maxmflops = 0.0;
  max_bw = 0.1;
  max_l3 = 0.1;
  phase = -1;
  old_flops = 0.0;
  old_oi = -1;
  ufreq = 0;
  old_decision = -1;
  old_ufreq = 0;
  reset_ufreq(*msr_fd);
  if(powercapping)
  {
	cur_powercap = default_powercap[0];
	cur_powercap1 = default_powercap[1];
  }
}

void
regulator_destroy(int msr_fd)
{
  reset_ufreq(msr_fd);
}

void reset_powercap( int socket )
{
  //  if(powercap_iter == 0){
  for(int i = N_PWRC_EVTS - 1; i >= 0; i--){
	DEBUGP("Reset powercap %lld %lld\n", read_powercap( socket, i), default_powercap[i]);
	write_powercap(socket, constraints[i], default_powercap[i]);
  }
  cur_powercap = default_powercap[0];
  cur_powercap1 = default_powercap[1];
  DEBUGP("Powercap reset is done at the same time as uncore frequency reset. However, the frequency set may be lower than the desired one because of powercap. retry_reset is used to try and reset uncore frequency once again\n");
  retry_reset = 1;
  just_reset_powercap = 1;
  deactivate_powercap = 0;
  //  }
}

void decrease_powercap( int socket)
{
  //  if(powercap_iter == 0){
  for(int i = N_PWRC_EVTS - 1; i >= 0; i--){
	DEBUGP("let's decrease the powercap from %lld to %lld (current read value %lld)\n", cur_powercap, cur_powercap - 5, read_powercap( socket, i ));

	write_powercap( socket, constraints[i], cur_powercap - 5);
  }
  cur_powercap -= 5;
  cur_powercap1 = cur_powercap;
  just_reset_powercap = 0;//we keep just_reset_powercap to 1 only if the previous iteration was reset
  //    }
}

void set_powercap_constraint1(int socket){
  write_powercap( socket, constraints[1], default_powercap[0]);
  cur_powercap1 = default_powercap[0];
  write_powercap( socket, constraints[0], cur_powercap);
  //  cur_powercap1 = default_powercap[0];
}

void increase_powercap( int socket)
{
  for(int i = N_PWRC_EVTS - 1; i >= 0; i--){
	DEBUGP("let's increase the powercap from %lld to %lld (current read value %lld)\n", cur_powercap, cur_powercap + 5, read_powercap( socket, i ));
	write_powercap( socket, i, cur_powercap + 5);
  }
  cur_powercap += 5;
  cur_powercap1 = cur_powercap;
  just_reset_powercap = 0;//we keep just_reset_powercap to 1 only if the previous iteration was reset
}


/** Decides for an action to take given an uncore id, the operational intensity
 * and the current mflops measured on that uncore. */
static enum action
decide_action(double oi, double mflops, int perf_loss, double l3bw)
{
  // don't do anything if there isn't enough work being produced
  if ((mflops < MIN_MFLOPS) && (phase == UNK)) {
    DEBUGP("Nothing happens: ");
    return SET_MIN_FREQ;
  }

  // phase detection
  // on phase change, reset uncore freq
  if (oi > 1 && phase != CPU) {
    deactivate_powercap = 0;
    maxmflops = mflops;
    max_bw = mflops / oi;
    max_l3 = l3bw;
    DEBUGP("Detected CPU-intensive phase (oi=%0.2f) ", oi);
    if(phase == UNK){
      /* A phase change after UNK most likely means that a program
		 started running. This implies that we do not need to set the
		 frequency to the maximum frequency but rather to decrease it
		 and see the impact*/
      DEBUGP("A new program was detected (oi=%0.2f) ", oi);
      phase = CPU;
      return  RESET_UFREQ;
    }
    else
	{
	  DEBUGP("Detected CPU-intensive phase (oi=%0.2f) flops %f", oi, maxmflops);
	  phase = CPU;
	  return RESET_UFREQ;
	}
  } else if (oi < 1 && phase != MEM) {
    maxmflops = mflops;
    max_bw = mflops / oi;
    max_l3 = l3bw;
    deactivate_powercap = 0;
    DEBUGP("Detected memory-intensive phase (oi=%0.2f) ", oi);
    if(phase == UNK){
      /* A phase change after UNK most likely means that a program
		 started running. This implies that we do not need to set the
		 frequency to the maximum frequency but rather to decrease it
		 and see the impact*/
      DEBUGP("A new program was detected (oi=%0.2f) ", oi);
      phase = MEM;
      return  RESET_UFREQ;
    }
    else
	{
	  DEBUGP("Detected memory-intensive phase (oi=%0.2f) flops %f", oi, maxmflops);
	  phase = MEM;
	  return RESET_UFREQ;
	}
  }

  if(perf_loss == 0)
  {
	DEBUGP("cur flops %f, decrease flops > %f, increase if flops < %f, oi %f old flops %f old oi %f maxflops %f\n", mflops, K_ERROR * maxmflops, K_ERROR * maxmflops, oi, old_flops, old_oi, maxmflops);
  }
  else
  {
	DEBUGP("cur flops %f, decrease flops > %f, increase if flops < %f, oi %f old flops %f old oi %f maxflops %f\n", mflops, (double) (100-perf_loss) /100. * maxmflops + (1. - K_ERROR) * maxmflops * 0.01,  (double) (100-perf_loss) /100. * maxmflops - (1. - K_ERROR) * maxmflops, oi, old_flops, old_oi, maxmflops);
  }

  if(oi > 100){
    if(powercapping){	
	  if (
	    (((mflops < K_ERROR * maxmflops) && (perf_loss == 0)) ||
	     ((mflops < (double) (100-perf_loss) /100. * maxmflops - (1. - K_ERROR) * maxmflops) && (perf_loss != 0))) ||
	    //	    ((l3bw < K_ERROR * max_l3) && (perf_loss == 0)) ||
	    //	    ((l3bw < (100.-perf_loss)/100. * max_l3 - (1. - K_ERROR) * max_l3) && (perf_loss != 0)) ||
	    (((mflops / oi) < K_ERROR * max_bw) && (perf_loss == 0)) ||
	    (((mflops /oi) < (100.-perf_loss)/100. * max_bw - (1. - K_ERROR) * max_bw) && (perf_loss != 0))){ /* The powercap impact the flops so much that we are below the tolerated slowdown, let's deactivate powercapping. The application flops variation may be below the tolerated slowdown, but an impact may still be observed as for EP. The L3 bandwidth seem to be a good indicator in this case for 5% and while memory bandwidth seems to be a good indicator for 10%*/
		DEBUGP("The flops, L3 bw or mem_bw dropped too much (cur l3 %f, max l3 %f, cur bw %f, max_bw %f)\n", l3bw, max_l3, mflops/oi, max_bw);
		if(cur_powercap1 != default_powercap[1])
		  deactivate_powercap = 1;
	  }
	  if((mflops / oi) < (0.75 * max_bw)){
		DEBUGP("The memory bandwidth behavior changed for a highly CPU-intensive applications, let's update max_bw (new_bw %f, old__bw %f)\n", mflops / oi, old_flops / old_oi);
		max_bw = mflops / oi;
	  }
	}
    return DEC_UFREQ;
  }


  if(mflops < K_ERROR * old_flops)
  {
	if (maxmflops <= mflops) {
	  DEBUGP("New observed old max %f new max%f\n", maxmflops, mflops);
	  maxmflops = mflops;
	}
	else if((old_decision == INC_UFREQ) || (old_decision == DO_NOTHING)){
	  DEBUGP("Increasing uncore freq did not improve performance. maxflops set to %f\n", mflops);
	  if(mflops < 0.5 * old_flops){
	    DEBUGP("Update max flops and decrease frequency %f\n", mflops);
	    maxmflops = mflops;
	    return DEC_UFREQ;
	  }
	  else
		if((powercapping) && 	((cur_powercap < default_powercap[0]) || (cur_powercap1 < default_powercap[1])))
		  /* The flops decreased despite decreasing ufreq, to make sure it is not due to powercap, let's increase powercap*/
		  return INC_PWRC;
		else return DO_NOTHING;
	}
	else
	  if((((mflops / oi) / (old_flops / old_oi)) / (mflops / old_flops) >= 0.9) && (((mflops / oi) / (old_flops / old_oi)) / (mflops / old_flops) <= 1.01)) /* this is roughly equivalent to saying : bw / flops == old_bw / old_flops : the interval [0.9, 1.01] is here because they will never be exacly equal to one another */
	  {
	    DEBUGP("The flops and the bandwidth dropped by roughly the same ratio, let's increase ufreq to make sure that the impact on the memory does not impact performance \n");
	    return INC_UFREQ;
	  }
	  else if((mflops / oi) / max_bw > (100.-perf_loss)/100.){// && ((l3bw / max_l3) > (100.-perf_loss)/100.)){
		DEBUGP("The flops dropped but the bandwidth dropped (old bw %f, new bw %f) by less than overhead percent compared to max bw\n", mflops/oi, max_bw);
		return DEC_UFREQ;
	  }
  }
  else
    if((mflops > 1.95 * old_flops) && (old_flops != 0.0))
	{
	  if(((mflops / oi) < (1.95 * (old_flops / old_oi))) && (l3bw < 1.95 * old_l3)) /* The flops increase but the memory bandwidth does not move*/
	  {
	    DEBUGP("The flops suddenly increase by a factor 2 and so did the intensity : decrease frequency old flops %f old oi %f\n", old_flops, old_oi);
	    reset_pc = 1;
	    return DEC_UFREQ;
	  }
	  else{
		DEBUGP("The flops suddenly increase by a factor 2 and the intensity remained constant : reset uncore freq old_flops %f old_oi %f\n", old_flops, old_oi);
		return RESET_UFREQ;
	  }
	
	}

      
  if (phase != UNK) {
    if (maxmflops <= mflops) {
      return DEC_UFREQ;
    } else if (((mflops > K_ERROR * maxmflops) && (perf_loss == 0))|| ((mflops > (double) (100-perf_loss) /100. * maxmflops + (1. - K_ERROR) * maxmflops) && (perf_loss !=0))){
      DEBUGP("Decide to decrease freq\n");
      return DEC_UFREQ;
    } else if (((mflops < K_ERROR * maxmflops) && (perf_loss == 0)) || ((mflops < (double) (100-perf_loss) /100. * maxmflops - (1. - K_ERROR) * maxmflops) && (perf_loss != 0))){      
      DEBUGP("Decide to increase freq\n");
      return INC_UFREQ;
    }
    else {
      return DO_NOTHING;
    }
  } else {
    return DO_NOTHING;
  }
}

void increase_reset_decrease_powercap(int socket_idx, double cur_power){
  if(cur_powercap != default_powercap[0]){
    DEBUGP("The flops dropped, let's reset powercap\n");
    increase_powercap(socket_idx);
  }
  else if(cur_powercap1 != default_powercap[1]){
    DEBUGP("The flops dropped, let's reset powercap\n");
    reset_powercap(socket_idx);
  }
  else if(cur_power < default_powercap[0] - 6.){
    if(just_reset_powercap)
	{
	  just_reset_powercap = 0;
	  set_powercap_constraint1(socket_idx);
	}
  }
}

void deactivate_powercap_function(int socket_idx){
  if(cur_powercap1 != default_powercap[1]){
    DEBUGP("OI > 200, the powercap impacted the performance let's increase it (cur %lld)\n", cur_powercap);
    reset_powercap(socket_idx);
  }
}


void handle_powercapping_at_inc_ufreq(int perf_loss, int socket_idx, double mflops, double cur_power, long long int cur_powercap){
  if(deactivate_powercap){ /* If the application has a high
							  oi, then the powercap
							  impacted the performance
							  rather than the uncore
							  frequency. As a consequence,
							  we keep the uncore frequency
							  steady and we increase
							  powercapping*/
    DEBUGP("Deactivate powercap for current iteration \n");
    deactivate_powercap_function(socket_idx);
 
  }
  else
    if(((mflops >= K_ERROR * maxmflops) && (perf_loss == 0)) || ((mflops >= (double) (100-perf_loss) /100. * maxmflops - (1. - K_ERROR) * maxmflops) && (perf_loss != 0))){
      DEBUGP("We decide to increase ufreq not because of flops drop, maybe we can decrease powercap\n");
      if ((cur_powercap > 65) && (!deactivate_powercap) && (!no_decrease))// && (cur_power < cur_powercap - 2))
	  {
		DEBUGP("The flops are ok (cur flops %f lower bound %f), let's decrease the powercap from %lld to %lld\n", mflops, (double) (100-perf_loss) /100. * maxmflops - (1. - K_ERROR) * maxmflops, cur_powercap, cur_powercap - 5);
		decrease_powercap(socket_idx);
	  }
    }
    else{
      DEBUGP("The flops are too low and the current power is high, let's increase if possible\n");
      increase_reset_decrease_powercap(socket_idx, cur_power);
    }
}


void handle_powercapping_at_dec_ufreq(int perf_loss, int socket_idx, double mflops, double cur_power, long long int cur_powercap){
  DEBUGP("Let's read powercap constraint 0 %lld\n", read_powercap( socket_idx, 0 ));
  if(deactivate_powercap){ /* If the application has a high
							  oi, then the powercap
							  impacted the performance
							  rather than the uncore
							  frequency. As a consequence,
							  we keep the uncore frequency
							  steady and we increase
							  powercapping*/
    DEBUGP("Deactivate powercap for current iteration \n");
    deactivate_powercap_function(socket_idx);
  }
  else
    if (reset_pc) /* We decrease ufreq but we increase powercap because there is a huge jump in flops*/
	{
	  if(cur_powercap1 != default_powercap[1]){
		DEBUGP("We decrease ufreq but we increase powercap because there is a huge jump in flops\n");
		DEBUGP("Increase powercap to maximum, as a consequence we call reset in order to increase constaint 1\n");
		reset_powercap(socket_idx);
	  }
	  reset_pc = 0;
	}
    else
      if(((mflops >= K_ERROR * maxmflops) && (perf_loss == 0)) || ((mflops >= (double) (100-perf_loss) /100. * maxmflops - (1. - K_ERROR) * maxmflops) && (perf_loss != 0))) {
		DEBUGP("Let's check if we can decrease powercap\n");
		if ((cur_powercap > 65) && (!deactivate_powercap) && (!no_decrease)) /* let's check if we can decrease powercap */
		{
		  DEBUGP("The flops are ok (cur flops %f lower bound %f), let's decrease the powercap from %lld to %lld\n", mflops, (double) (100-perf_loss) /100. * maxmflops - (1. - K_ERROR) * maxmflops, cur_powercap, cur_powercap - 5);
		  decrease_powercap(socket_idx);
		}
		else
		  DEBUGP("Powercap is already at min, keep steady\n");
      }
      else{
		DEBUGP("The flops are too low and the current power is high, let's increase if possible\n");
		increase_reset_decrease_powercap(socket_idx, cur_power);
      }
}


/** Iterates on all the uncores, decides for an action to take, and does it,
 * while logging a few things. */
void
regulate(struct data* data, int perf_loss, int socket_idx, int msr_fd)
{
  double oi = get_meter(data, METER_OP_INT);
  double mflops = get_meter(data, METER_FLOPS);
  double l3bw = get_meter(data, METER_L3_BW);
  int cur_freq =  get_ufreq(msr_fd);
  double cur_power =  get_meter(data, METER_PKG_PWR);

  if((maxmflops < mflops) && (mflops < 1000000)){
    DEBUGP("New observed old max %f new max%f\n", maxmflops, mflops);
    maxmflops = mflops;
  }
  if((mflops / oi > max_bw) && (mflops < 1000000))
    max_bw = mflops / oi;
  if((l3bw > max_l3) && (l3bw < 1000000))
    max_l3 = l3bw;

  no_decrease = 0;
  DEBUGP("New iteration\n");
  if(powercapping)
    DEBUGP("Let's read powercap c0 %lld (cur_powercap %lld) c1 %lld (cur_powercap1 %lld)\n", read_powercap( socket_idx, 0 ), cur_powercap, read_powercap( socket_idx, 1 ), cur_powercap1);
  if(power)
    data->sleep_usec = data->default_usleep;
  if(cpu_frequency)/* the arguments from the command line indicate that the uncore frequency should be printed*/
    data->meters[ METER_FREQ ] = get_freq(socket_idx ) /  1000000; //freq_getCpuClockCurrent(socket_idx) / 1000000; //

  /* Firs let's see if the past powercapping decision did not impact uncore frequency value*/
  if((powercapping) && (retry_reset) && (old_decision != DO_NOTHING) && (old_decision != DEC_UFREQ))
  {
	DEBUGP("Previous decision of resetting powercap may have impacted the decision of increasing or resetting ufreq, let's check and correct. Requested ufreq %d set ufreq %d\n", ufreq, cur_freq);
	if((old_decision == RESET_UFREQ)  && (cur_freq != MAX_UFREQ)){
	  DEBUGP("Reset ufreq\n");
	  set_ufreq(MAX_UFREQ, msr_fd);
	  data->meters[ METER_UFREQ] =  ufreq;
	  retry_reset = 0;
	  goto finished;
	}
	else{ //old_decision = INC_UFREQ
	  if(old_decision == INC_UFREQ){
		if(ufreq != cur_freq){
		  DEBUGP("Increase ufreq\n");
		  set_ufreq(ufreq, msr_fd);
		  data->meters[ METER_UFREQ] =  ufreq;
		  retry_reset = 0;
		  goto finished;
		}
	  }
	}
  }
  DEBUGP("requested ufreq %d, set ufreq %d\n", ufreq,  cur_freq);
  ufreq = cur_freq;
  /* Then let's see if there are no flops. In this case, we leave uncore frequency at maximum but decrease the powercap*/
  if((mflops < 1000000) && (l3bw < 1000000)) 
  {
	if((oi < 0.02) && (phase == MEM)) {
	  DEBUGP("there are very few flops compared to memory bandwidth, let's keep uncore frequency steady\n");
	  if(perf_loss == 0)
	  {
		DEBUGP("cur flops %f, decrease flops > %f, increase if flops < %f, oi %f old flops %f old oi %f maxflops %f\n", mflops, K_ERROR * maxmflops, K_ERROR * maxmflops, oi, old_flops, old_oi, maxmflops);
	  }
	  else
	  {
		DEBUGP("cur flops %f, decrease flops > %f, increase if flops < %f, oi %f old flops %f old oi %f maxflops %f\n", mflops, (double) (100-perf_loss) /100. * maxmflops + (1. - K_ERROR) * maxmflops * 0.01,  (double) (100-perf_loss) /100. * maxmflops - (1. - K_ERROR) * maxmflops, oi, old_flops, old_oi, maxmflops);
	  }

	  if ((powercapping) && (cur_powercap > 65) && (!no_decrease))
		decrease_powercap(socket_idx);
	  if(ufreq != MAX_UFREQ)
		set_ufreq(MAX_UFREQ, msr_fd);
	  old_decision = DO_NOTHING;
	}
	else if((oi >= 0.02) && (old_oi < 0.02) && (old_oi != -1)) {
	  DEBUGP("in the previous period, there were very few flops, let's sleep for longer to allow the application to reach a steady state\n");
	  if(powercapping)
		reset_powercap(socket_idx);
	  data->sleep_usec = 3 * data->default_usleep;

	}
	else{/* We're done with special cases, let's handle the general behavior */
	  if(powercapping)
		if(cur_power > cur_powercap)
	    {
	      DEBUGP("current power (%f) larger than powercap constraint1 (%lld)\n", cur_power, cur_powercap1);
	      if(cur_power >= default_powercap[0])
			reset_powercap(socket_idx);
	      else
			increase_powercap(socket_idx);
	      no_decrease = 1;
	    }
	  DEBUGP("Socket #%d: flops %f  intensity %f\n", socket_idx, mflops, oi);

	  enum action action = decide_action(oi, mflops, perf_loss, l3bw);
	  DEBUGP("decide action\n");
	  switch (action) {

	  case RESET_UFREQ:
		DEBUGP("Reset ufreq\n");
		if(ufreq != MAX_UFREQ)
		  set_ufreq(MAX_UFREQ, msr_fd);
		data->meters[ METER_UFREQ] =  ufreq;
		data->sleep_usec = data->default_usleep;
		old_decision = RESET_UFREQ;
		if(powercapping)
	    {
	      if(cur_powercap1 != default_powercap[1])
		  {
			DEBUGP("Reset powercap\n");
			reset_powercap(socket_idx);
		  }
	    }
	    
		break;
	  case INC_UFREQ:
		if (ufreq + STEP_UFREQ <= MAX_UFREQ) {
		  DEBUGP("increase: ");
		  old_ufreq = ufreq;
		  set_ufreq(ufreq + STEP_UFREQ, msr_fd);
		  data->meters[ METER_UFREQ] =  ufreq;
		  if(maxmflops >= 4 * mflops)
			/* We're in the same phase, the the flop
			   dropped. This is fixed when we reached the max
			   possible ufreq, but in case we do not have enough
			   time let's fix it here as well*/
			maxmflops = mflops;
		  data->sleep_usec = data->default_usleep;
		  old_decision = INC_UFREQ;
		} else
	    {
	      DEBUGP("Cannot increase: already at max %f\n", mflops);
	      if((mflops < 0.5 * old_flops) || ( (mflops/oi) >= ((1 + K_ERROR) * (old_flops / old_oi))))
		  {
			DEBUGP("Update maxflops\n");
			maxmflops = mflops;
		  }
	      old_ufreq = ufreq;
	      set_ufreq(ufreq - STEP_UFREQ, msr_fd);
	      data->meters[ METER_UFREQ] =  ufreq;
	      data->sleep_usec = data->default_usleep;
	      old_decision = DO_NOTHING;
	    }
		if(powercapping)
		  handle_powercapping_at_inc_ufreq(perf_loss, socket_idx, mflops, cur_power, cur_powercap);
	    
		break;
	  case DEC_UFREQ:
		if (ufreq - STEP_UFREQ >= MIN_UFREQ) {
		  if(old_ufreq == ufreq - STEP_UFREQ){
			DEBUGP("It seems like we have reached a stabilization phase current ufreq %d desired ufreq %d \n", ufreq, ufreq - STEP_UFREQ);
			if((data->sleep_usec < 10 * data->default_usleep)
			   && (!power) && (!powercapping)) /* if we want to measure the power, let's not increase the sleep time. If powercapping is used, we cannot increase the interval to avoid any impact on performance*/
			  data->sleep_usec = data->sleep_usec * 2;
			old_ufreq = 0;
			old_decision = -1;
		  }
		  else{
			DEBUGP("decrease: ");
			old_ufreq = ufreq;
			set_ufreq(ufreq - STEP_UFREQ, msr_fd);
			data->meters[ METER_UFREQ] =  ufreq;
			data->sleep_usec = data->default_usleep;
			old_decision = DEC_UFREQ;
		  }
	    
	      
		}
		else
	    {
	      DEBUGP("Cannot dercease: already at min %f\n", mflops);
	      old_decision = DO_NOTHING;
	      if((data->sleep_usec < 10 * data->default_usleep)
			 && (!power)) /* if we want to measure the power, let's not increase the sleep time*/
			data->sleep_usec = data->sleep_usec * 2;
	    }
	  
		if(powercapping)
		  handle_powercapping_at_dec_ufreq(perf_loss, socket_idx, mflops, cur_power, cur_powercap);
		//	    }

		break;
	  case DO_NOTHING:
		/* Increase sleep intervall until it reaches a certain limit
		 * which is 10x the default the default interval*/
		old_decision = DO_NOTHING;	  
		if((data->sleep_usec < 10 * data->default_usleep) && (mflops > 0.01)
		   && (!power) && (!powercapping)) /* if we want to measure the power, let's not increase the sleep time. And if we handle powercapping, we do not sleep longer to avoir any negative impact on performance*/
		  data->sleep_usec = data->sleep_usec * 2;  
		DEBUGP("Do nothing ! %f\n", mflops);
		break;
	  case SET_MIN_FREQ:
		DEBUGP("No flops, set freq to min\n");
		set_ufreq(MIN_UFREQ, msr_fd);
		data->meters[ METER_UFREQ] =  ufreq;
		break;
	  case INC_PWRC:
		old_decision = DO_NOTHING;
		if(powercapping)
	    {
	      if(cur_powercap != default_powercap[0]){
			increase_powercap(socket_idx);
	      }
	      else if(cur_powercap1 != default_powercap[1])
			reset_powercap(socket_idx);
	    }
		break;
	  }
	    
	}
  finished:
	old_flops = mflops;
	old_oi = oi;
	old_l3 = l3bw;
  }
  else
  {
	DEBUGP("error in the flops measurements, skip to next iteration however we must update constraint1 in case the current power consumption is high since power measurements are correct\n");
	if(powercapping){
	  if(cur_power <  default_powercap[0] - 6.){
	    if(just_reset_powercap)
	      {
		DEBUGP("Wrong: We have just reset powercap, and the current power consumption is low, let's set powercap constraint1 to 125\n");
		just_reset_powercap = 0;
		set_powercap_constraint1(socket_idx);
	      }
	  }
	  else
	    if(cur_power > cur_powercap)
	      {
		DEBUGP("This is an iteration where flops measurements were wrong, we however check if the current power consumption is larger than powercap and reset\n");

		reset_powercap(socket_idx);
		no_decrease = 1;
	      }
	}
  }
    
}
