/*
Copyright (C) Amina Guermouche 2019.
Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
(See accompanying file LICENSE file or copy at http://opensource.org/licenses/MIT)
*/
void init_msr(int* msr_fd, int socket);
int read_msr(int msr_fd, uint32_t uncore_register);
int write_msr(uint64_t freq, int msr_fd);
void destroy_msr(int msr_fd);
