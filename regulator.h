/*
Copyright (C) Amina Guermouche 2019.
Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
(See accompanying file LICENSE file or copy at http://opensource.org/licenses/MIT)
*/

#include <stdint.h>
#include "duf.h"

/** Inits the regulator and resets uncore frequency on all uncores.
 * @param n_uncores number of uncores that will be metered.
 */
void
regulator_init(int *msr_fd);

/** Destroys the regulator and resets uncore frequency on all uncores */
void
regulator_destroy(int msr_fd);

/** Regulates the uncores by reading meter_val from data.
    This needs to be called after calculating the meters.
*/
void
regulate(struct data* data, int perf_loss, int socket_idx, int msr_fd);

uint64_t get_freq(const int cpu_id );
