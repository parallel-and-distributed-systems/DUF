# duf

## Usage
~~~~
Usage: duf [OPTION...]

  -c, --powercap             Use DUF for dynamic powercapping
  -f, --frequency            Enable CPU frequency output.
  -l, --maximum 	     performance loss=perf_loss (Maximum tolerated performance loss in %).
  -m, --measurement          Use DUF for measurements only.
  -p, --power                Disable sleep for longer than the period because power measurement is required.
  -s, --sockets=uncore_list  Comma-separated list of sockets / package indexes to monitor.
  -t, --sleeptime=time       Time in seconds between two measurments.
  -v, --verbose              Enable debug output.
  -?, --help                 Give this help list
  --usage                    Give a short usage message
  -V, --version              Print program version

Mandatory or optional arguments to long options are also mandatory or optional
for any corresponding short options.
~~~~
## Requirements
The powercap library is needed to compile DUF (https://github.com/powercap/powercap.git).

## Installation

You'll need a working installation of PAPI and hwloc. The makefile
also requires pkg-config to find these. The msr module needs to be
loaded as well.

Then type e.g. `make arch=XeonE5-2620V4` to build for the Broadwell-EP
architecture. For now three architectures are available:
XeonE5-2620V4, XeonE5-2680V4 and XeonGold6130. More details on how to generate these files are provided in arch/README.md

`make install` installs the program to `/usr/local/bin`, this can be set by modifying PREFIX.

## Example usage
Meter on sockets 0 and 1 every 10ms and log output into out.tsv
~~~~
duf -s 0,1 -t 0.01 > out.tsv 
~~~~

## Troubleshooting
  - You'll likely need to disable hyperthreading to get reliable results.
  - You need root access to use this tool.
  - A distributed version of DUF is in progress. As a consequence, DUF cannot be used on a distributed machine.
  - If duf can't start, please check that the events used are availiable using `papi_native_avail`. If not, you may need to activate the msr kernel module and to allow acces to linux perf events by executing (as root) :
  ~~~~
  modprobe msr
  echo -1 >/proc/sys/kernel/perf_event_paranoid
  ~~~~

