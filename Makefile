CFLAGS = -Wall -Werror -Wstrict-overflow -Wextra -fno-strict-aliasing -g -march=native -O0
PREFIX=/usr/local

obj = duf.o meters.o args.o regulator.o msr_access.o powercap_access.o
LDFLAGS += `pkg-config --libs papi hwloc` -lpowercap
CFLAGS += `pkg-config --cflags papi hwloc`

MPI_CFLAGS= `pkg-config --cflags ompi`
MPI_LDFLAGS= `pkg-config --libs ompi` -lmpi

ifndef arch
$(error arch is not set. Please specify it using make arch=<your arch>)
endif

obj += arch/$(arch).o

all: duf libmpi-duf.so

%.o: %.c %.h
	$(CC) $(CFLAGS) -c -o $@ $<

duf: $(obj) 
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

libmpi-duf.so: mpi-duf.o
	$(CC) -shared -o libmpi-duf.so mpi-duf.o $(LDFLAGS)  $(MPI_LDFLAGS)  -ldl
mpi-duf.o: mpi-duf.c
	$(CC) -fPIC -c mpi-duf.c $(MPI_CFLAGS)

.PHONY: clean install uninstall
clean:
	rm -f $(obj) duf libmpi-duf.so mpi-duf.o

install: install-duf install-duf-mpi

install-duf: duf
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp $< $(DESTDIR)$(PREFIX)/bin/duf
install-duf-mpi: libmpi-duf.so
	mkdir -p $(DESTDIR)$(PREFIX)/lib
	cp $< $(DESTDIR)$(PREFIX)/lib/libmpi-duf.so

uninstall:
	rm -f $(PREFIX)/bin/duf
