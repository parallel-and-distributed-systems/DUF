/*
Copyright (C) Amina Guermouche 2019.
Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
(See accompanying file LICENSE file or copy at http://opensource.org/licenses/MIT)
*/

#include <argp.h>
#include <stdlib.h>
#include "args.h"
#include "duf.h"

const char* argp_program_version = "duf version " DUF_VERSION;
const char* argp_program_bug_address = "https://gitlab.com/parallel-and-distributed-systems/DUF";
static struct argp_option options[] = {
  { "sleeptime",
    't',
    "time",
    0,
    "Time in seconds between two measurments.",
    0 },
  { "sockets",
    's',
    "uncore_list",
    0,
    "Comma-separated list of sockets / package indexes to meter.",
    0 },
    { "maximum performance loss",
    'l',
    "perf_loss",
    0,
    "Maximum tolerated performance loss in %.",
    0 },
  { "frequency", 'f', 0, 0, "Enable CPU frequency output.", 0 },
  { "power", 'p', 0, 0, "Disable sleep for longer than the period because power measurement is required.", 0 },
  { "measurement", 'm', 0, 0, "Use DUF for measurements only.", 0 },
  { "powercap", 'c', 0, 0, "Use DUF for dynamic powercapping", 0 },
  { "verbose", 'v', 0, 0, "Enable debug output.", 0 },
  { 0 }
};


static error_t
parse_opt(int key, char* arg, struct argp_state* state)
{
  struct arguments* arguments = state->input;
  switch (key) {
    case 't': {
      double x = atof(arg);
      if (x <= 0.0)
        DIE("Invalid value for -t\n");
      arguments->sleep_usec = x * 1000000.0;
      break;
    }
    case 's': {
      arguments->uncore_list = arg;
      break;
    }
    case 'l': {
      arguments->perf_loss = atoi(arg);
      break;
    }
    case 'v': {
      arguments->verbose = true;
      break;
    }
  case 'f': {
      arguments->cpu_frequency = true;
      break;
    }
  case 'p': {
      arguments->power = true;
      break;
    }
  case 'm': {
      arguments->measurement = true;
      break;
    }
  case 'c': {
    arguments->powercapping = true;
    break;
  }
    case ARGP_KEY_ARG:
      return 0;
    default:
      return ARGP_ERR_UNKNOWN;
  }
  return 0;
}

struct argp argp = { options, parse_opt, 0, 0, 0, 0, 0 };
