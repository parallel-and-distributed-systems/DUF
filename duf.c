/*
Copyright (C) Amina Guermouche 2019.
Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
(See accompanying file LICENSE file or copy at http://opensource.org/licenses/MIT)
*/

#include <argp.h>
#include <hwloc.h>
#include <papi.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include "duf.h"
#include "arch/event.h"
#include "args.h"
#include "meters.h"
#include "regulator.h"
#include "msr_access.h"
#include "powercap_access.h"

extern struct argp argp;
/** global verbose flag. If true, DEBUGP() prints. */
int verbose = 0;
int cpu_frequency = 0; /* the cpu frequency is printed at every time step*/
int power = 0; /* the power is measured by default. However, if power
		  measurement at every time step is required, then we
		  cannot allow duf to sleep for more than the timestep
		  (as shown in regulator.c)*/

int measurement = 0; /*DUF is used to measure power and frequency */
int powercapping = 0; /*Dynamic power capping */
long long int default_powercap[N_PWRC_EVTS];
long long int default_powercap_c1 = 0;
int powercap_counter = 0;
int powercap_iter = 0;

powercap_rapl_pkg * pkgs;

volatile bool notKilled; /* Used to free memory when CTRL-C is used*/

void signal_callback_handler(int signum) {
    DEBUGP("Program stopped %d\n", signum);
    notKilled = false;
}

/** Prepares an event given its name template and a cpu index, and adds it to
 * the given EventSet. If it fails to add it, it tries again after activating
 * multiplex. If it fails again, the progam ends.
 * @param EventSet EventSet index.
 * @param name event name template, will be sprintfed with cpu
 * @param cpu index of the cpu
 */
static void
add_event(int EventSet, const char* name, int cpu)
{
  char buf[255];
  int code;
  int retval;
  PAPI_event_info_t info;

  sprintf(buf, name, cpu);
  retval = PAPI_event_name_to_code(buf, &code);
  PAPIDIEREASON(retval, "event_name_to_code");

  retval = PAPI_get_event_info(code, &info);
  PAPIDIEREASON(retval, "get_event_info");
  DEBUGP("Adding %s (datatype=%d, freq=%d)\n",
         buf,
         info.data_type,
         info.update_freq);
  retval = PAPI_add_event(EventSet, code);
  if (retval != PAPI_OK) {
    DEBUGP("Activating multiplex\n");
    retval = PAPI_set_multiplex(EventSet);
    PAPIDIE(retval);
    retval = PAPI_add_named_event(EventSet, buf);
    PAPIDIE(retval);
  }
}
/** Helper function to add the target arch's cpu events */
static void
add_cpu_events(int EventSet, int cpu)
{
  for (int i = 0; i < N_CPU_EVTS; i++) {
    add_event(EventSet, CPU_EVT_NAMES[i], cpu);
  }
}

/** Helper function to add the target arch's uncore events */
static void
add_uncore_events(int EventSet, int uncore)
{
  for (int i = 0; i < N_UNCORE_EVTS; i++) {
    add_event(EventSet, UNCORE_EVT_NAMES[i], uncore);
  }
}

static void
init_data(struct data* data, int cpus, int usleep)
{
  data->cpus = cpus;
  data->cpu_val = calloc(N_CPU_EVTS * cpus, sizeof(long long));
  data->uncore_val = calloc(N_UNCORE_EVTS, sizeof(long long));
  data->meters = calloc(N_METERS, sizeof(double));
  data->sleep_usec = usleep;
  data->default_usleep = usleep;
}

static void
destroy_data(struct data* data)
{
  free(data->cpu_val);
  free(data->uncore_val);
  free(data->meters);
}

int
main(int argc, char** argv)
{
  int EventSetCPU = PAPI_NULL;
  int EventSetUncore = PAPI_NULL;
  int retval;
  struct arguments arguments;
  notKilled = true;
  arguments.sleep_usec = 1000000; // 1s default
  arguments.verbose = false;
  arguments.cpu_frequency = false;
  arguments.power = false;
  arguments.measurement = false;
  arguments.powercapping = false;
  arguments.perf_loss = 0;
  arguments.uncore_list = "";
  argp_parse(&argp, argc, argv, 0, 0, &arguments);
  assert(arguments.perf_loss <= 100);
  // make verbose state global
  if (arguments.verbose)
    verbose = 1;

  if (arguments.cpu_frequency)
    cpu_frequency = 1;

  if (arguments.power)
    power = 1;

  if (arguments.measurement)
    measurement = 1;

  if (arguments.powercapping)
    powercapping = 1;

  /* Initialize PAPI */
  if (PAPI_library_init(PAPI_VER_CURRENT) != PAPI_VER_CURRENT) {
    perror("unable to initialize PAPI");
    exit(1);
  }
  retval = PAPI_multiplex_init();
  PAPIDIEREASON(retval, "cannot init multiplex");

  // create the CPU eventset and set options
  retval = PAPI_create_eventset(&EventSetCPU);
  PAPIDIEREASON(retval, "cannot create CPU eventset\n");

  // tell PAPI that the events on this eventset will be using the CPU component
  // (#0). This is required for setting options. If we added events to this
  // eventset before setting options, PAPI would already know that the eventset
  // would be using the CPU component, so this wouldn't be needed. But some
  // options can only be set to an empty eventset...
  retval = PAPI_assign_eventset_component(EventSetCPU, 0);
  PAPIDIEREASON(retval, "assign cpu eventset");

  // Set the domain as high as it will go, to measure absolutely everything
  // that's happening on the machine. Not sure about what this exactly does, but
  // i'm not taking any chances. This can only be done to an empty eventset
  // Setting to ALL or another high value (eg KERNEL) requires root access
  PAPI_option_t options;
  options.domain.eventset = EventSetCPU;
  options.domain.domain = PAPI_DOM_ALL;
  retval = PAPI_set_opt(PAPI_DOMAIN, &options);
  PAPIDIEREASON(retval, "set opt PAPI_DOMAIN");

  // Set granularity to "current CPU". That is to say, core, (PU in hwloc
  // terms). By default PAPI only reads what's happening in the current thread
  // (this program). We want everything that's happening on the core in our
  // case. There is also a PAPI_set_cmp_granularity function that can be called
  // before creating eventsets, but for some reason it prevents us from adding
  // events belonging to different cpus. This works tho
  options.granularity.eventset = EventSetCPU;
  options.granularity.granularity = PAPI_GRN_SYS;
  retval = PAPI_set_opt(PAPI_GRANUL, &options);
  PAPIDIEREASON(retval, "set opt PAPI_GRANUL");

  // Create the Uncore event set and set options.
  // What we are measuring on these are physically happening on the whole
  // socket, so domain or granularity options are not needed.
  retval = PAPI_create_eventset(&EventSetUncore);
  PAPIDIEREASON(retval, "cannot create Uncore eventset\n");

  int cpus = 0;
  int uncores = 0;
  // parse uncore list and determine on which cores we're going to meter stuff
  // TODO error handling (bad input)
  if (!strcmp(arguments.uncore_list, "")) {
    // TODO if no arguments are given, the program should detect and enable all
    // availiable uncores.
    DIE("Socket detection not implemented, please use -s\n");
  }
  hwloc_topology_t topology;
  if (hwloc_topology_init(&topology) == -1)
    DIE("Error while initializing hwloc topology module\n");
  if (hwloc_topology_load(topology) == -1)
    DIE("Error while loading topology\n");
  char* ptr = strtok(arguments.uncore_list, ",");
  int socket = atoi(ptr);
  while (ptr != NULL) {
    int uncore = atoi(ptr);
    add_uncore_events(EventSetUncore, uncore);
    hwloc_obj_t cpu_obj = hwloc_get_obj_below_by_type(
      topology, HWLOC_OBJ_PACKAGE, uncore, HWLOC_OBJ_PU, 0);
    if (cpu_obj == NULL)
      DIE("No cores found on socket %d, aborting.\n", uncore);
    int idx = 0;
    DEBUGP("Socket %d: metering on PUs no ", uncore);
    while (cpu_obj) {
      DEBUGP("%d ", cpu_obj->os_index);
      add_cpu_events(EventSetCPU, (int)cpu_obj->os_index);
      cpus++;
      idx++;
      cpu_obj = hwloc_get_obj_below_by_type(
        topology, HWLOC_OBJ_PACKAGE, uncore, HWLOC_OBJ_PU, idx);
    }
    DEBUGP("\n");
    uncores++;
    ptr = strtok(NULL, ",");
  }

  struct data data;
  init_data(&data, cpus, arguments.sleep_usec);

  /* msr */
  int msr_fd;
  init_msr(&msr_fd, socket);
  init_powercap();
  /* powercap */
  //  int powercap_fds [N_PWRC_EVTS];
  if(powercapping)
    {
      //      init_powercap(powercap_fds, socket);
      for(int i = 0; i < N_PWRC_EVTS; i++){
	default_powercap[i] = read_powercap( socket, constraints[i]);
	DEBUGP("default powercap values for socket %d is %lld\n", socket, default_powercap[i]);
      }
      //POWERCAP_RAPL_CONSTRAINT_LONG
      
      //      write_powercap(powercap_fds[1], 115);
      // write_powercap(powercap_fds[0], 115);
      /* 	printf("Powercap valute constraint %d is %lld\n", 0, read_powercap( powercap_fds[0] )); */
      /* 	//      } */
      /* //       write_powercap(powercap_fds[0], 125);  */
      /* /\* for(int i = 0; i < N_PWRC_EVTS; i++){ *\/ */
      /* /\* 	printf("Powercap valute constraint %d is %lld\n", i, read_powercap( powercap_fds[i] )); *\/ */
      /* /\* } *\/ */
    }

  /*setting up CTRL-C*/
  signal(SIGINT, signal_callback_handler);
  
  long long t0, t1, t;
  double dt;

  LOGP("duf version " DUF_VERSION " starting\n");
  LOGP("Metering on %d sockets (%d PUs per socket) every %d µs\n",
       socket,
       data.cpus,
       arguments.sleep_usec);
  // start the counters
  retval = PAPI_start(EventSetCPU);
  PAPIDIEREASON(retval, "Cannot start CPU meters");
  retval = PAPI_start(EventSetUncore);
  PAPIDIEREASON(retval, "Cannot start uncore meters");
  // start time measurment
  t = t0 = PAPI_get_real_usec();
  print_meters_header();
  int i = 0;
  /* Daemon */
  if(measurement){  
    while (notKilled) {
      usleep(data.sleep_usec);
          // reset arrays
      for (int i = 0; i < N_CPU_EVTS * data.cpus; i++)
	data.cpu_val[i] = 0.0;
      for (int i = 0; i < N_UNCORE_EVTS; i++)
	data.uncore_val[i] = 0.0;
      // read counters and reset them
      PAPI_accum(EventSetCPU, data.cpu_val);
      PAPI_accum(EventSetUncore, data.uncore_val);
      uint32_t uncore_register = 0x621;
      uint64_t ufreq = read_msr(msr_fd, uncore_register);
      ufreq = (ufreq & 0xFFULL) * 100;
      data.meters[ METER_UFREQ] =  ufreq;
      if(cpu_frequency)/* the arguments from the command line indicate that the uncore frequency should be printed*/
	data.meters[ METER_FREQ ] = get_freq(socket ) /  1000000;
      // read time
      t1 = PAPI_get_real_usec();
      dt = (double)(t1 - t) / 1000000.0;
      t = t1;
      DEBUGP("t=%lld, dt = %f\n", t1, dt);
      // calculate meters for each uncore
      calculate_meters(&data, dt);
      print_meters(&data, (double)(t - t0) / 1000000.0);
    }
  }
  else
    {
      regulator_init(&msr_fd);
      usleep(2*data.sleep_usec);
      DEBUGP("usleep %d default_usleep %d\n", data.sleep_usec, data.default_usleep);
      while (notKilled) {
	i ++;
	powercap_iter ++;
	if(powercap_iter == 5)
	  powercap_iter = 0;
	usleep(data.sleep_usec);
	//powercap_counter ++; 
	DEBUGP("usleep %d default_usleep %d\n", data.sleep_usec, data.default_usleep);
	// reset arrays
	for (int i = 0; i < N_CPU_EVTS * data.cpus; i++)
	  data.cpu_val[i] = 0.0;
	for (int i = 0; i < N_UNCORE_EVTS; i++)
	  data.uncore_val[i] = 0.0;
	// read counters and reset them
	PAPI_accum(EventSetCPU, data.cpu_val);
	PAPI_accum(EventSetUncore, data.uncore_val);
	// read time
	t1 = PAPI_get_real_usec();
	dt = (double)(t1 - t) / 1000000.0;
	t = t1;
	DEBUGP("t=%lld, dt = %f\n", t1, dt);
	// calculate meters for each uncore
	calculate_meters(&data, dt);
	data.meters[ METER_POWERCAP_C0 ] = read_powercap( socket, 0 );
	data.meters[ METER_POWERCAP_C1 ] = read_powercap( socket, 1 );
	print_meters(&data, (double)(t - t0) / 1000000.0);
	regulate(&data, arguments.perf_loss, socket, msr_fd);
	/* if((powercap_counter <= 3) && (powercap_counter >= 1)){/\* we assume that the powercap interval is 5 times the uncore frequency interval*\/ */
	/*   DEBUGP("aaaaaaaaaaaa %d\n", powercap_counter); */
	/*   powercap_counter ++; */
	/* } */
	/* else */
	/*   powercap_counter = 0; */
	
      }
      regulator_destroy(msr_fd);
    }
  destroy_msr(msr_fd);
  if(powercapping)
    {
      for(int i = N_PWRC_EVTS - 1; i >= 0; i--){
      	write_powercap(socket, i, default_powercap[i]);
      	DEBUGP("resetting powercap at the end\n");
      }
      //      destroy_powercap(powercap_fds);
    }
  destroy_data(&data);
  DEBUGP("Terminating\n");
  return 0;
}
