/*
Copyright (C) Amina Guermouche 2019.
Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
(See accompanying file LICENSE file or copy at http://opensource.org/licenses/MIT)
*/

/* -*- c-file-style: "GNU" -*- */
#define _GNU_SOURCE

#include <dlfcn.h>
#include <mpi.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <sched.h>

#define SCAVENGER_BINARY_DEFAULT "/home/trahay/Soft/opt/DUF/PowerScavenger/PS"
#define SCAVENGER_PERIOD_DEFAULT "0.1"
#define SCAVENGER_SOCKET_LIST_DEFAULT "0"
#define SCAVENGER_OUTPUT_FILE_DEFAULT "scavenger_output.log"
#define SCAVENGER_LOCKFILE_DEFAULT "scavenger.lock"

/* pointers to actual MPI functions (C version)  */
int (*libMPI_Init)(int *, char ***);
int (*libMPI_Init_thread)(int *, char ***, int, int*);
int (*libMPI_Finalize)(void);

/* fortran bindings */
void (*libmpi_init_)(int*e);
void (*libmpi_init_thread_)(int*, int*, int*);
void (*libmpi_finalize_)(int*);

pid_t duf_pid = 0;
MPI_Comm shmcomm;

static void start_duf() {
  /* if several MPI processes run on a machine, one of them is in
     charge of launching DUF on all the scokets */
  MPI_Comm_split_type(MPI_COMM_WORLD, MPI_COMM_TYPE_SHARED, 0,
		      MPI_INFO_NULL, &shmcomm);
  int shmrank;
  MPI_Comm_rank(shmcomm, &shmrank);
  if(shmrank == 0) {

    /* a lockfile is used for synchronizing processes: once the
     * duf/scavenger process is initialized, it creates the lockfile.
     *
     * The MPI process thus needs to wait until the lockfile exist
     * befor resuming
     */
    char* lockfile=getenv("SCAVENGER_LOCKFILE");
    if(!lockfile) {
      printf("SCAVENGER_LOCKFILE environment variable not set. Using the default value (%s)\n", SCAVENGER_LOCKFILE_DEFAULT);
      lockfile = SCAVENGER_LOCKFILE_DEFAULT;
    }

    struct stat lock_stat;
    if(stat(lockfile, &lock_stat) == 0){
      /* the lock file already exists. delete it */
      unlink(lockfile);
    }
    
    duf_pid = fork();
    if(!duf_pid) {
      /* DUF process */
      char* ps=getenv("SCAVENGER_BINARY");
      if(!ps) {
	printf("SCAVENGER_BINARY environment variable not set. Using the default value (%s)\n", SCAVENGER_BINARY_DEFAULT);
	ps = SCAVENGER_BINARY_DEFAULT;
      }

      char* period=getenv("SCAVENGER_PERIOD");
      if(!period) {
	printf("SCAVENGER_PERIOD environment variable not set. Using the default value (%s)\n", SCAVENGER_PERIOD_DEFAULT);
	period = SCAVENGER_PERIOD_DEFAULT;
      }

      char* socket_list=getenv("SCAVENGER_SOCKET_LIST");
      if(!socket_list) {
	printf("SCAVENGER_SOCKET_LIST environment variable not set. Using the default value (%s)\n",
	       SCAVENGER_SOCKET_LIST_DEFAULT);
	socket_list=SCAVENGER_SOCKET_LIST_DEFAULT;
      }

      char* scavenger_output=getenv("SCAVENGER_OUTPUT_FILE");
      if(!scavenger_output) {
	printf("SCAVENGER_OUTPUT_FILE environment variable not set. Using the default value (%s)\n",
	       SCAVENGER_OUTPUT_FILE_DEFAULT);
	scavenger_output = SCAVENGER_OUTPUT_FILE_DEFAULT;
      }
      int fd = open(scavenger_output, O_TRUNC|O_CREAT|O_WRONLY, S_IRUSR|S_IWUSR);
      if(fd < 0) {
	fprintf(stderr, "Cannot open file %s: %s\n", scavenger_output, strerror(errno));
	abort();
      }

      sleep(1);
      printf("[%d] running command '%s -t %s -s %s 2>&1 > %s'\n",
	     getpid(), ps, period, socket_list, scavenger_output);
      dup2(fd, 1);
      dup2(fd, 2);
      execlp(ps, ps, "-t", period,  "-s", socket_list, NULL);
      fprintf(stderr, "Failed to run %s: %s\n", ps, strerror(errno));
      abort();
    }

    while(stat(lockfile, &lock_stat) < 0 && errno == ENOENT){
      sched_yield();
    }
    printf("%s does exist !\n", lockfile);
    printf("duf_pid = %d\n", duf_pid);
    unlink(lockfile);
  }

  MPI_Barrier(shmcomm);
}

int MPI_Init(int* argc, char***argv) {
  int retval;
  retval = libMPI_Init(argc, argv);
  start_duf();
  return retval;
}

int MPI_Init_thread(int* argc, char***argv, int required, int* provided) {
  int retval;
  retval = libMPI_Init_thread(argc, argv, required, provided);
  start_duf();
  return retval;
}

void mpi_init_(void *error) {
  libmpi_init_((int*) error);
  start_duf();
}


void mpi_init_thread_(int *r, int *p, int *error) {
  libmpi_init_thread_(r, p, error);
  start_duf();
}


static void stop_duf() {
  /* wait until all the local MPI process reach mpi_finalize */
  MPI_Barrier(shmcomm);

  if(duf_pid > 0 ){
    printf("Killing process %d\n", duf_pid);
    kill(duf_pid, SIGKILL);
  }
}


void mpi_finalize_(int *error) {
  stop_duf();
  libmpi_finalize_(error);
}


int MPI_Finalize() {
  stop_duf();
  libMPI_Finalize();
}


static void __duf_init(void) __attribute__ ((constructor));
static void __duf_init(void) {
  libMPI_Init = dlsym(RTLD_NEXT, "MPI_Init");
  libMPI_Init_thread = dlsym(RTLD_NEXT, "MPI_Init_thread");
  libMPI_Finalize = dlsym(RTLD_NEXT, "MPI_Finalize");
  libmpi_init_ = dlsym(RTLD_NEXT, "mpi_init_");
  libmpi_init_thread_ = dlsym(RTLD_NEXT, "mpi_init_thread_");
  libmpi_finalize_ = dlsym(RTLD_NEXT, "mpi_finalize_");
}


static void __duf_conclude(void) __attribute__ ((destructor));
static void __duf_conclude(void) {

}
