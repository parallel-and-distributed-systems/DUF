#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "msr_access.h"

void init_msr(int* msr_fd, int socket)
{
  char* msr_name = (char*)malloc(15);
    sprintf(msr_name, "/dev/cpu/%d/msr", socket);
    *msr_fd = open(msr_name, O_RDWR);
    if (*msr_fd < 0)
      {
	fprintf(stderr,"Cannot open MSR file %s\n", msr_name);
	exit(0);
      }
    free(msr_name);
}
  

int read_msr(int msr_fd, uint32_t uncore_register)
{
  ssize_t ret;
  uint64_t freq = 0;
  if (msr_fd > 0)
    {
      ret = pread(msr_fd, &freq, sizeof(uint64_t), uncore_register);
      if (ret < 0)
        {
	  fprintf(stderr,"Cannot read uncore frequency register\n");
	  if (msr_fd > 0)
	    {
	      close(msr_fd);
	    }
	  exit(0);
        }
      else if (ret != sizeof(uint64_t))
        {
	  fprintf(stderr, "Incomplete read of uncore frequency register \n");
	  if (msr_fd > 0)
	    {
	      close(msr_fd);
	    }
	  exit(0);
        }
    }
  return freq;
}

int write_msr(uint64_t freq, int msr_fd)
{
  ssize_t ret;
  uint32_t uncore_register = 0x620;
  if (msr_fd > 0)
    {
      ret = pwrite(msr_fd, &freq, sizeof(uint64_t), uncore_register);
      if (ret < 0)
        {
	  fprintf(stderr, "Cannot write uncore frequency register\n");
	  if (msr_fd > 0)
	    {
	      close(msr_fd);
	    }
	  exit(0);
        }
      else if (ret != sizeof(uint64_t))
        {
	  fprintf(stderr, "Incomplete read on register\n");
	  if (msr_fd > 0)
	    {
	      close(msr_fd);
	    }
	  exit(0);
        }
    }
  return 1;
}

void
destroy_msr(int msr_fd)
{
  if (msr_fd > 0)
    {
      close(msr_fd);
    }
}
